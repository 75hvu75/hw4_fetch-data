'use strict'

const filmsUrl = 'https://ajax.test-danit.com/api/swapi/films';
const filmsList = document.querySelector('#films-list');

fetch(filmsUrl)
    .then(response => response.json())
    .then(films => {
        films.forEach(film => {
            const filmItem = document.createElement('li');

            filmItem.innerHTML = `<strong>Episode ${film.episodeId}: ${film.name}</strong><br>${film.openingCrawl}`;

            const characterList = document.createElement('ul');

            characterList.id = 'characters-list';

            characterList.classList.add('hidden');

            filmItem.appendChild(characterList);

            filmItem.addEventListener('click', () => {
                characterList.classList.toggle('hidden');

                if (!characterList.classList.contains('hidden')) {
					Promise.all(film.characters.map(characterUrl => fetch(characterUrl)
						.then(response => response.json())))
						.then(characters => {
                        characterList.innerHTML = '';

                        characters.forEach(character => {
                            const characterItem = document.createElement('li');

                            characterItem.textContent = character.name;

                            characterList.appendChild(characterItem);
                        });
                    });
                }
            });

            filmsList.appendChild(filmItem);
        });
    });
